<?php

/**
*@package story.php
*
*BLA BL ABLA BLA BLA BLA
*/
/**
*@package story 
*
*/
class story extends resource{
	/**
	*Constructor
	*/
 	var $new = 1;
	var $table = 'story'; 

	
	function story(){
		
		//object props array
		$this->props=array(
			'story_id' =>  array('value' => '',
							'datatype' => 'id',
							'view' => '1',
							'edit' => '1',
							'map' => 'story.story_id'
							),
			'title' => array('value' => '',
							'datatype' => 'text',
							'view' => '1',
							'edit' => '1',
							'map' => 'story.title'
								),
			'body' => array(
							'value' => '',
							'datatype' => 'textarea',
							'view' => '0',
							'edit' => '1',
							'map' => 'story.body'
							),
			'user_id' => array(
							'value' => '',
							'datatype' => 'id',
							'map' => 'story.user_id',
							'view' => '0',
							'edit' => '1'
			),
			'time_posted' => array(
							'datatype' => 'datetime',
							'view' => '1',
							'edit' => '1',
							'map' => 'story.time_posted'
							),
			'homepage' => array(
              'datatype' => 'oneofmany'
              )
			);
	}

  function count_comments(){

  }
}

?>
