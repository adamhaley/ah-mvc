<?php

/**
*@package comment.php
*
*BLA BL ABLA BLA BLA BLA
*/
/**
*@package comment 
*
*/
class comment extends resource{
	/**
	*Constructor
	*/
 	var $new = 1;
	var $table = 'comment'; 	

	
	function comment(){
		
		//object props array
		$this->props=array(
			'comment_id' =>  array('value' => '',
							'datatype' => 'id',
							'view' => '1',
							'edit' => '1',
							'map' => 'comment.comment_id'
							),
			'comment_parent_id' => array('value' => '',
							'datatype' => 'id',
							'view' => '1',
							'edit' => '1',
							'map' => 'comment.comment_parent_id'
								),
			'story_photo_id' => array(
							'value' => '',
							'datatype' => 'id',
							'view' => '1',
							'edit' => '1',
							'map' => 'comment.story_photo_id'
							),
			'body' => array(
							'value' => '',
							'datatype' => 'textarea',
							'map' => 'comment.body',
							'view' => '0',
							'edit' => '1'
			),
			'user_id' => array(
						
							'datatype' => 'id',
							'map' => 'comment.user_id',
							'view' => '0',
							'edit' => '1'
							),
			'date_posted' => array(
					
							'datatype' => 'datetime',
							'view' => '1',
							'edit' => '1',
							'map' => 'comment.date_posted'
							),
			'story_id' => array(
              'datatype' => 'id',
              'map' => 'comment.story_id',
              'view' => '0',
              'edit' => '1'
              ),
        'num_replies' => array(
            'datatype' => "text"
        )
       
			);
	}

  function get_associated_user(){
    $robj = new user;
    if($uid = $this->get_prop('user_id')){
      $robj->load($uid);
      return $robj;
    }else{
      return false;
    }
  }

  function get_replies(){
  }
}

?>
