<?php

/**
*BLA BL ABLA BLA BLA BLA
*/
class story_photo extends resource{
	/**
	*Constructor
	*/
	var $table = 'story_photo'; 	


	function story_photo(){
		 $new = 1;
		//object props array
		$this->props=array(
			'story_photo_id' =>  array('value' => '',
							'datatype' => 'id',
							'view' => '1',
							'edit' => '1',
							'map' => 'story_photo.story_photo_id'
							),
			'story_id' => array('value' => '',
							'datatype' => 'id',
							'view' => '1',
							'edit' => '1',
							'map' => 'story_photo.story_id'
								),
			'filename' => array(
							'value' => '',
							'datatype' => 'text',
							'view' => '1',
							'edit' => '1',
							'map' => 'story_photo.filename'
							),
			'caption' => array(
							'value' => '',
							'datatype' => 'text',
							'map' => 'story_photo.caption',
							'view' => '0',
							'edit' => '1'
							),
			'description' => array(
						
							'datatype' => 'textarea',
							'map' => 'story_photo.description',
							'view' => '0',
							'edit' => '1'
							),
			'user_id' => array(
					
							'datatype' => 'id',
							'view' => '0',
							'edit' => '1',
							'map' => 'story_photo.user_id'
							),
			'time_posted' => array(

                                                        'datatype' => 'datetime',
                                                        'view' => '1',
                                                        'edit' => '1',
                                                        'map' => 'story_photo.time_posted'
                                                        )
						);
	}	
	
	function create_thumb($filename,$imagepath){
                $thumbname = 'thumb_' . $filename;
                $fullpath = $imagepath . $filename;

                list($width,$height) = getimagesize($fullpath);

                $newheight = 100;
                $ratio = $height / $newheight;

                $newwidth = $width / $ratio;
                $thumb = imagecreatetruecolor($newwidth, $newheight);
                $source = imagecreatefromjpeg($fullpath);

                // Resize
                imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                $newfullpath = $imagepath . $thumbname;

                // Output
                imagejpeg($thumb, $newfullpath, 70);

                imagedestroy($thumb);
                imagedestroy($source);

        }

        function create_resized($filename,$imagepath){
                $thumbname = 'resized_' . $filename;
                $fullpath = $imagepath . $filename;

                list($width,$height) = getimagesize($fullpath);

                $newheight = 480;
                $ratio = $height / $newheight;

                $newwidth = $width / $ratio;
                $thumb = imagecreatetruecolor($newwidth, $newheight);
                $source = imagecreatefromjpeg($fullpath);

                // Resize
                imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                $newfullpath = $imagepath . $thumbname;

                // Output
                imagejpeg($thumb, $newfullpath, 70);

                imagedestroy($thumb);
                imagedestroy($source);

        }

	/**
        *Deletes this resource.Returns true on sucess, false on failure^M
        */
        function delete(){
                global $db;
                global $message;
                        
                $file = $this->sql_file_name('delete');
        
       
                
                $tobj = new template($file);
                $sql = $tobj->parse_key("uid",$_SESSION['uid']);
                
                $sql = $tobj->parse_resource($this); 
                $sql = $tobj->get_source();
                
	        if(unlink('images/' . $this->get_prop('filename'))){
			$message->add("confirmation",$this->get_prop('filename') . " deleted.");
		}else{
			$message->add("error","could not delete " . $this->get_prop('filename'));
		}

                if($sql){
                        return$db->query($sql);
                }else{
                                $message->add("error","empty sql from $file");  
                }
         
		       
        }
	
}
?>
