<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="headlinks">
<link rel="stylesheet" href="css/main.css"  />
<link rel="stylesheet" href="../global/css/global.css" />
<!-- this has to be the 1st stylesheet due
     stylesheet[2] -->
<link rel="stylesheet" href="../global/skins/cobalt/css/main.css"  />

<!-- skinnable behaviors -->
<script type="text/javascript" src="../global/skins/cobalt/js/style.js"></script>

<!-- calendar stylesheet -->
<link rel="stylesheet" type="text/css" media="all" href="../global/js/jscalendar/skins/aqua/theme.css" title="Aqua" />

<!-- main calendar program -->
<script type="text/javascript" src="../global/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="../global/js/jscalendar/lang/calendar-en.js"></script>
<script type="text/javascript" src="../global/js/jscalendar/calendar-setup.js"></script>

<!-- Import Other Packages -->
<script language="javascript"   src="../global/js/debug.js" />
<script language="javascript"   src="../global/js/javascript.js" />
<script language="javascript"   src="../global/js/selector.js" />

<!-- Import Prototype/Scriptaculous/Behavior -->
<script src="../global/js/prototype.js" type="text/javascript"></script>
<script src="../global/js/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<script src="../global/js/behaviour.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript" src="../../../global/js/tiny_mce/tiny_mce.js"></script>
</xsl:template>
</xsl:stylesheet>
