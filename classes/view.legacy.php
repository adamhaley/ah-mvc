<?
/**
/*@package view
*/
/**
/*@package view
*/
class view{
	var $document;
	var $stylesheet;
	var $resources;

	
	function build($resourcearray=array(),$auxarray=array()){
		global $buildpath;
		global $path;
		$this->document = new DOMDocument('1.0', 'UTF-8');

		
		$this->mode = $_SESSION['mode']? $_SESSION['mode'] : 'live';
				
		//$this->document->formatOutput = true;
		
		
		$xml = '';
		$resxml = "<resources>\n";
		if(is_array($resourcearray)){
			foreach($resourcearray as $robj){
			 $resxml .= $robj->get_props_xml_use_attributes() . "\n";

			}
		}
		$resxml .= "</resources>\n";
		/*
		//process resource xml through security filter
		$xsl = DomDocument::load($path . "global/xsl/securityfilter.xsl");    

                $xslproc = new XSLTProcessor();
                $xslproc->importStylesheet($xsl);
                $resxml = $xslproc->transformToXML($resxml);
		*/
		global $message;
		
		//BUILD XML STRING
		
		$xml = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE page SYSTEM "../global/dtds/page.dtd">			
<?xml-stylesheet href="xsl/' . $this->stylesheet . '" type="text/xsl"?>
<page>
  <session>
		' . $this->array_to_xml($_SESSION) . ' 
   
  </session>
  <request>
   ' . $this->array_to_xml(array('res' => $_REQUEST['res'])) . $this->array_to_xml($_GET) . $this->array_to_xml($_POST) . '
  </request>

  ' . $message->get_xml() . ' 
  <aux>
  ' . $this->array_to_xml($auxarray) . '
  </aux>
  <navigation>
  ' .  $this->build_navigation() . '
  </navigation>
  <content>
 ' . $resxml . '
  </content>
</page>';
		
		//END BUILD XML STRING

		//encode xml utf8
		$xml = utf8_encode($xml);
		
		$dom = $this->document;
                $dom->loadXML($xml);		

		//if in debug mode, send xml to browser
		if($this->mode=='debug'){
			
			header("content-type: text/xml");
			//replace styleshee
			return  $dom->saveXML();

		
		//else proscess xsl and send html to browser
		}else if($this->mode=='live'){
			header("content-type: text/html");

			
			return $this->process_xsl($dom);
		}
	}
	
	/**
	*Turns an associative array into a string xml fragment
	*/
	function array_to_xml($array = array()){
		$out = "";
		if(is_array($array)){
			foreach($array as $key => $value){	
				$value = str_replace("&","&amp;",$value);
				$value = str_replace("<","&lt;",$value);
				$value = str_replace(">","&gt;",$value);
			
				$out .= "<$key>";
				$out .= $value;
				$out .= "</$key>\n";
			}
		}
		return $out;
	}
	
	/**
	*sets string xsl stylesheet filename
	*/
	function set_stylesheet($path){
		if(!$this->stylesheet){
			$this->stylesheet = $path;
		}	
	}
	
	/**
	*Takes string xml and processes with xsl stylesheet, returning output
	*/
	function process_xsl($xml){
		global $buildpath;
		$xslpath = $buildpath . "xsl/" . $this->stylesheet;
		$xsl = DomDocument::load($xslpath);  	
	
		$xslproc = new XSLTProcessor();
		$xslproc->importStylesheet($xsl);
		return $xslproc->transformToXML($xml);
	}
	
	function add_request_var($key,$value){
		$dom = $this->document;
		$reqnode = $dom->getElementByTagName("request");
		$reqnode->appendChild($dom->createElement($key,$value));	
		$this->document = $dom;
	}
	
	function build_navigation(){

	}

}

class view_admin extends view{

	function build_navigation(){
		$tobj = new template('XML/adminnav.xml');
		$nav = $tobj->get_source();
		$tobj = new template('XML/admin_subnav.xml');
		$nav .= $tobj->get_source();
		return $nav;
	}
}

class view_admin_browse_user extends view{

	function build_navigation(){
		$tobj = new template('XML/adminnav.xml');
		$nav = $tobj->get_source();
		$tobj = new template('XML/admin_browse_user_subnav.xml');
		$nav .= $tobj->get_source();
		return $nav;
	}
}

class view_admin_smart_ftp extends view{

	function build_navigation(){
		$tobj = new template('XML/adminnav.xml');
		$nav =  $tobj->get_source();
		$tobj = new template('XML/ftp_subnav.xml');
		$nav .= $tobj->get_source();
		return $nav;
	}

}


?>
